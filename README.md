# Matrix docker-compose config

I wasn't satisified with the docker-compose provided by the matrix project for synapse so I wrote my own hoping it might be useful to someone.

## Generate configuration

This will generate a homeserver.yaml file based on the environment settings.

    $ docker-compose run --rm synapse_confing migrate_config

## Run containers

    $ docker-compose up -d

## Configure

Look at [this file](https://github.com/matrix-org/synapse/blob/master/docker/conf/homeserver.yaml) to see all the available environment variables.

Then edit docker-compose.yml and change the environment setting under ``synapse_config``.

### Edit configuration

By default docker volumes are created and used, to find out where the config is run ``docker volume inspect matrix-compose_synapse-data``.

Here's a one-liner that shows you the path of homeserver.yaml if you have jq installed.

    $ ls -la "$(docker volume inspect matrix-compose_synapse-data | jq -r '.[0].Mountpoint')/homeserver.yaml"

# More to come

Did the following quickly before a dentist appointment but planning to setup riot-web and provide instructions on how to forward traffic from internet to these contains.

The goal is that anyone with control over their home router/firewall should be able to run this on their laptop to setup their own federated instance, and riot-web client to use locally.
